# Testautomation Workshop

This project is meant as a testobject which mimics a real microservice architecture setup.

Please see the wiki for instructions on how to build and run.

## My PoC for the TestCoders casus
I've created a simple API test suite with Postman which can be further extended with Newman for CI purposes.

The following changes were made in regards to the original repo:
* The Docker Compose has lines added for building the API test suite
* The folder /api_test has been added, including a Dockerfile and Postman collection containing all the test cases
* The Docker image in the /api_test folder adds a fresh Newman installation. Besides a local Postman installation, tests can also be run with Newman on a CLI (see also 'Guide' below)
* For CI extension a .circleci map has been created with a .yml file containing build instructions for CircleCI

### Guide
Run the build by using `docker-compose build && docker-compose up -d`

Run tests in the /api_test folder with command `newman run TestCodersPOC.postman_collection.json`

Additionaly the Postman collection can also be obtained from https://www.getpostman.com/collections/6d1500e416d2c2acb1da
